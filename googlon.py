#!/usr/bin/python

class Googlon(object):
    alphabet = "rklnwqxfjcdgtvbmspzh"
    letters_foo = ['h', 'z', 'b', 'f', 'c']
    letters_bar = []
    
    def __init__(self):
        self.letters_bar = [l for l in self.alphabet
                                        if l not in self.letters_foo]
    
    def is_preposition(self, word):
        if (len(word) == 5 and word[-1] in self.letters_foo and 'f' not in word):
            return True
        return False
    
    def is_verb(self, word):
        if len(word) >= 7 and word[-1] in self.letters_bar:
            return True
        return False
    
    def is_verb_fp(self, word):
        return (self.is_verb(word) and word[0] in self.letters_bar)
    
    def sort_words(self, words):
        return sorted(words, key=lambda word: [self.alphabet.index(c) for c in word])
    
    def convert_to_decimal(self, word):
        portuguese_alphabet = '0123456789abcdefghijklmnopqrstuvwxyz'
        list_values = [self.alphabet.index(c) for c in word]
        number = ''.join([portuguese_alphabet[n] for n in list_values])[::-1]
        return long(number, 20)
        
    def is_nice_number(self, word):
        decimal = self.convert_to_decimal(word)
        if decimal >= 674422 and not decimal % 3:
            return True
        return False