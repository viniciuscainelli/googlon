#!/usr/bin/python

from googlon import Googlon

text = "nrmjjnd ssgzz rrfrk fmjqrd cfs bnhpzd whkg csdtpqzt skkxt jkmrlb \
mtcc ppc lhpnl vdlnrkm fxlbs gnrcdr djpnrdx jvmcfkql vrxlrk twq nrdml dshtqnl \
nmhrkbqw zvdzws spdfmtk hztvj hflhhcqd kgfnbvp rzhr vmvx mgshtsdf ckhwmfq jdf \
ncrhx ddtz qhvr qfjgxh ndz kbqlspjs gbw cpzsnwqg dhgpmp pcvnxwv dhxwqjxw rwglb \
bggdfzt flc mtxlvfwf pjgfkkd qsxbl vjdwdd qcmb hxtmnf nbnxbghs pzvjj dgw drv hpn \
dqbmsvg fvkw wpsppnl rghswcnq qwcpdjq tzhtt dnjnmlr flqplb xfz ldmmgbn jxsdmcpb"
words = text.split()

googlon = Googlon()

preposicoes = filter(lambda word: googlon.is_preposition(word), words)
verbos = filter(lambda word: googlon.is_verb(word), words)
verbos_fp = filter(lambda word: googlon.is_verb_fp(word), words)
texto_ordenado = ' '.join(googlon.sort_words(words))
numeros_bonitos = filter(lambda word: googlon.is_nice_number(word), words)

print "preposicoes:", len(preposicoes)
print "verbos:", len(verbos)
print "verbos primeira pessoa:", len(verbos_fp)
print "texto ordenado:", texto_ordenado
print "numeros bonitos:", len(numeros_bonitos)